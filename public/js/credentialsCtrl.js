var app = angular.module('paionline')

app.controller('CredentialsCtrl', [ '$http', 'routes', 'common', function($http, routes, common) {
    console.log('Kontroler CredentialsCtrl startuje')
    var ctrl = this

    ctrl.visible = function() {
        var route = routes.find(function(el) { return el.route == '/credentials' })
        return route && route.roles.includes(common.sessionData.role)
    }
    if(!ctrl.visible()) return

    ctrl.credentials = []

    ctrl.newCredential = {
        person_id: "0",
        password: '',
        role: 0
    }

    var refreshCredentials = function() {
        $http.get('/credential').then(
            function(res) {
                ctrl.credentials = res.data
            },
            function(err) {}
        )
    }

    var refreshCredential = function() {
        $http.get('/credential?_id=' + ctrl.credentials[ctrl.selected]._id).then(
            function(res) {
                ctrl.credential = res.data
            },
            function(err) {}
        )
    }

    refreshCredentials();

    ctrl.insertNewData = function() {
        $http.post('/credential', ctrl.newCredential).then(
            function(res) {
                refreshCredentials()
            },
            function(err) {}
        )
    }

    ctrl.select = function(index) {
        ctrl.selected = index
        refreshCredential()
    }

    ctrl.updateData = function() {
        $http.put('/credential?_id=' + ctrl.credentials[ctrl.selected]._id, ctrl.credential).then(
            function(res) {
                refreshCredentials();
            },
            function(err) {}
        )
    }

    ctrl.deleteData = function() {
        $http.delete('/credential?_id=' + ctrl.credentials[ctrl.selected]._id).then(
            function(res) {
                refreshCredentials();
            },
            function(err) {}
        )
    }

}])